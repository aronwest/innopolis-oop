﻿note
	author: "Timur Kasatkin"

class
	PERSON

create
	make

feature -- Initialization

	make (a_name: STRING)
			-- Create a person named -a_name-
		require
			a_name_valid: a_name /= Void and then not a_name.is_empty
		do
			name := a_name
			coworkers := create {LINKED_LIST [PERSON]}.make
		ensure
			name_set: name = a_name
			coworkers /= Void
		end

feature -- Access

	name: STRING

	coworkers: LIST [PERSON]

	has_flu: BOOLEAN

feature -- Element change

	add_coworker (p: PERSON)
		require
			p_exists: p /= Void
			p_different: p /= Current
			p_not_already_in_coworkers: not coworkers.has (p)
		do
			coworkers.force (p)
		end

	del_coworker (p: PERSON)
		require
			p_exists: p /= Void
			p_different: p /= Current
			p_in_coworkers: coworkers.has (p)
		do
			coworkers.prune (p)
		end

	set_coworkers (persons: LIST [PERSON])
		require
			persons_exists: persons /= Void
			persons_not_include_current: not persons.has (Current)
		do
			coworkers := persons
		end

	set_flu
			-- Set -has_flu- to True.
		do
			has_flu := True
		ensure
			has_flu: has_flu
		end

invariant
	name_valid: name /= Void and then not name.is_empty
	coworkers /= Void and then not coworkers.has (Current)

end
