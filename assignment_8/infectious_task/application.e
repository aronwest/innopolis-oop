﻿note
	description: "assignment_8 application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

create
	make

feature {NONE} -- Initialization

	make
		local
			joe, mary, tim, sarah, bill, cara, adam: PERSON
		do
			create joe.make ("Joe")
			create mary.make ("Mary")
			create tim.make ("Tim")
			create sarah.make ("Sarah")
			create bill.make ("Bill")
			create cara.make ("Cara")
			create adam.make ("Adam")
			joe.add_coworker (sarah)
			sarah.add_coworker (joe)
			adam.add_coworker (joe)
			joe.add_coworker (adam)
			tim.add_coworker (sarah)
			sarah.add_coworker (tim)
			sarah.add_coworker (cara)
			cara.add_coworker (sarah)
			bill.add_coworker (tim)
			tim.add_coworker (bill)
			cara.add_coworker (mary)
			mary.add_coworker (cara)
			mary.add_coworker (bill)
			bill.add_coworker (mary)
			infect (bill)
				--			joe.set_coworker (sarah)
				--			adam.set_coworker (joe)
				--			tim.set_coworker (sarah)
				--			sarah.set_coworker (cara)
				--			bill.set_coworker (tim)
				--			cara.set_coworker (mary)
				--			mary.set_coworker (bill)
				--			infect (bill)
		end

	infect (p: PERSON)
			-- Infect `p' and coworkers.
		require
			p_exists: p /= Void
		do
			p.set_flu
			across
				p.coworkers as i
			loop
				if not i.item.has_flu then
					infect (i.item)
				end
			end
		end

		--	infect (p: PERSON)
		--			-- Infect ‘p’ and coworkers.
		--		require
		--			p_exists: p /= Void
		--		do
		--			p.set_flu
		--			if p.coworker /= Void and then not p.coworker.has_flu then
		--				infect (p.coworker)
		--			end
		--		end

end
