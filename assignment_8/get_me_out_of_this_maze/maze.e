class
	MAZE

inherit

	ARRAY2 [CHARACTER]
		redefine
			out
		end

create
	make

feature -- Map characters

	free_path: CHARACTER = '-'
			-- Character for empty fields.

	an_exit: CHARACTER = '#'
			-- Character for an exit field.

	a_wall: CHARACTER = '*'
			-- Character for a wall field.

	Visited_char: CHARACTER = 'x'
			-- Character for a field that has been visited by `find_path'.

feature -- Element change

	set_empty (r, c: INTEGER)
			-- Set field with row `r' and column `c' to empty.
		require
			r_valid: r >= 1 and r <= height
			c_valid: c >= 1 and c <= width
		do
			put (free_path, r, c)
		ensure
			field_set: item (r, c) = free_path
		end

	set_exit (r, c: INTEGER)
			-- Set field with row `r' and column `c' to exit.
		require
			r_valid: r >= 1 and r <= height
			c_valid: c >= 1 and c <= width
		do
			put (an_exit, r, c)
		ensure
			field_set: item (r, c) = an_exit
		end

	set_wall (r, c: INTEGER)
			-- Set field with row `r' and column `c' to wall.
		require
			r_valid: r >= 1 and r <= height
			c_valid: c >= 1 and c <= width
		do
			put (a_wall, r, c)
		ensure
			field_set: item (r, c) = a_wall
		end

	set_visited (r, c: INTEGER)
			-- Set field with row `r' and column `c' to visited.
		require
			r_valid: r >= 1 and r <= height
			c_valid: c >= 1 and c <= width
		do
			put (Visited_char, r, c)
		ensure
			field_set: item (r, c) = Visited_char
		end

feature -- Status report

	is_valid (c: CHARACTER): BOOLEAN
			-- Is `c' a valid map character?
		do
			Result := c = free_path or c = a_wall or c = an_exit
		end

feature -- Path finding

	path: STRING
			-- Sequence of instructions to find the way out of the maze.

	find_path (r, c: INTEGER)
			-- Find the path starting at row `r' and column `c'.
		do
			if path_found (r, c) then
				path := ""
				build_path (r, c)
			end
		end

feature {NONE} --Implementation

	build_path (r, c: INTEGER)
		do
			if is_exit (r, c) then
				path := path + "You're free!"
			else
				set_empty (r, c)
				if is_reachable (r - 1, c) and (is_visited (r - 1, c) or is_exit (r - 1, c)) then
					path := path + "N > "
					build_path (r - 1, c)
				elseif is_reachable (r, c + 1) and (is_visited (r, c + 1) or is_exit (r, c + 1)) then
					path := path + "E > "
					build_path (r, c + 1)
				elseif is_reachable (r + 1, c) and (is_visited (r + 1, c) or is_exit (r + 1, c)) then
					path := path + "S > "
					build_path (r + 1, c)
				elseif is_reachable (r, c - 1) and (is_visited (r, c - 1) or is_exit (r, c - 1)) then
					path := path + "W > "
					build_path (r, c - 1)
				end
			end
		end

	path_found (r, c: INTEGER): BOOLEAN
		do
			if not is_reachable (r, c) or is_visited (r, c) then
				Result := false
			elseif is_exit (r, c) then -- check whether we found solution
				Result := true
			else
				-- mark cell (r,c) as part of path
				set_visited (r, c)
				if path_found (r - 1, c) or else path_found (r, c + 1) or else path_found (r + 1, c) or else path_found (r, c - 1) then
					Result := true
				else
					-- unmark cell (r,c)
					set_empty (r, c)
					Result := false
				end
			end
		end

	is_reachable (r, c: INTEGER): BOOLEAN
		do
			Result := is_inside (r, c) and not is_wall (r, c)
		end

	is_inside (r, c: INTEGER): BOOLEAN
		do
			Result := (1 <= r and r <= height) and (1 <= c and c <= width)
		end

	is_wall (r, c: INTEGER): BOOLEAN
		do
			Result := Current [r, c] = a_wall
		end

	is_exit (r, c: INTEGER): BOOLEAN
		do
			Result := Current [r, c] = an_exit
		end

	is_free (r, c: INTEGER): BOOLEAN
		do
			Result := Current [r, c] = free_path
		end

	is_visited (r, c: INTEGER): BOOLEAN
		do
			Result := Current [r, c] = visited_char
		end

feature -- Output

	out: STRING
			-- Maze map.
		local
			i, j: INTEGER
		do
			from
				i := 1
				j := 1
				Result := ""
			until
				i > height
			loop
				from
					j := 1
				until
					j > width
				loop
					Result.append_character (item (i, j))
					j := j + 1
				end
				i := i + 1
				Result := Result + "%N"
			end
		end

end
