class
	MAZE_APPLICATION

inherit

	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
		local
			maze: MAZE
			maze_reader: MAZE_READER
			r, c: INTEGER
		do
			io.putstring ("Please enter the name of a maze file: ")
			io.read_line
			create maze_reader
			maze_reader.read_maze (io.laststring)
			if maze_reader.has_error then
				io.putstring (maze_reader.error_message)
			else
				maze := maze_reader.last_maze
				io.putstring ("%N" + maze.out + "%N")
				io.putstring ("Please enter a starting field for finding path.%N")
				io.putstring ("Row: ")
				io.read_integer
				r := io.last_integer
				io.putstring ("Column: ")
				io.read_integer
				c := io.last_integer
				maze.find_path (r, c)
				if maze.path /= Void then
					io.putstring ("There's a way out! Go " + maze.path + "%N")
				else
					io.putstring ("Oops, no way out! You're trapped!%N")
				end
			end
		end

end
