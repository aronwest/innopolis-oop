class
	ORDERING [G -> COMPARABLE]

feature

	ordering_list (l: ARRAYED_LIST [G]): ARRAYED_LIST [G]
		require
			l_exists: l /= Void
		do
			Result := merge_sort (l)
		end

feature {NONE} --Implementation

	merge_sort (l: ARRAYED_LIST [G]): ARRAYED_LIST [G]
		local
			middle_ind: INTEGER
			left, right: ARRAYED_LIST [G]
		do
			if l.count <= 1 then
				Result := l
			else
				middle_ind := l.count // 2
				l.go_i_th (1)
				left := l.duplicate (middle_ind)
				l.go_i_th (middle_ind+1)
				right := l.duplicate (middle_ind + 1)

				left := merge_sort (left)
				right := merge_sort (right)
				Result := merge (left, right)
			end
		end

	merge (left, right: ARRAYED_LIST [G]): ARRAYED_LIST [G]
		local
			result_list: ARRAYED_LIST [G]
			i, j: INTEGER
		do
			create result_list.make (left.count + right.count)
			from
				i := 1
				j := 1
			until
				i > left.count or j > right.count
			loop
				if left [i] <= right [j] then
					result_list.force (left [i])
					i := i + 1
				else
					result_list.force (right [j])
					j := j + 1
				end
			end
			if i > left.count then
				from
				until
					j > right.count
				loop
					result_list.force (right [j])
					j := j + 1
				end
			end
			if j > right.count then
				from
				until
					i > left.count
				loop
					result_list.force (left [i])
					i := i + 1
				end
			end
			Result := result_list
		end

end
