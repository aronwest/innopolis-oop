note
	description: "ordering_cards application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

create
	make

feature {NONE} -- Initialization

	int_count: INTEGER = 10

	make
		local
			int_ordering: ORDERING [INTEGER]
			cards_ordering: ORDERING [CARD]
			int_list: ARRAYED_LIST [INTEGER]
			deck: DECK
			i: INTEGER
		do
			create int_ordering
			create cards_ordering
			create int_list.make (int_count)
			from
				i := 0
			until
				i >= int_count
			loop
				int_list.force (int_count - i)
				i := i + 1
			end
			io.putstring ("Before sort: " + int_list.out)
			int_list := int_ordering.ordering_list (int_list)
			io.putstring ("After sort: " + int_list.out)
			create deck.make_full
			io.putstring (deck.out)
			io.putstring(cards_ordering.ordering_list (deck).out)
		end

end
