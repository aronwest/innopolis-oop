note
	description: "The class represents a single card from a French deck."
	author: "Timur Kasatkin"

class
	CARD

inherit

	COMPARABLE
		redefine
			out
		end

create
	make

feature {NONE}

	make (c_suit: CARD_SUIT; c_face: CARD_FACE)
		require
			c_suit_exists: c_suit /= Void
			c_face_exists: c_face /= Void
		do
			suit := c_suit
			face := c_face
		ensure
			suit = c_suit
			face = c_face
		end

feature --Access

	suit: CARD_SUIT

	face: CARD_FACE

feature --Comparison

	is_less alias "<" (other: like Current): BOOLEAN
		do
			if suit = other.suit then
				Result := face < other.face
			else
				Result := suit < other.suit
			end
		ensure then
			suit = other.suit implies Result = (face < other.face)
			suit /= other.suit implies Result = (suit < other.suit)
		end

	out: STRING
		do
			Result := suit.name + " " + face.name
		end

invariant
	suit /= Void
	face /= Void

end
