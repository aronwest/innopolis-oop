note
	author: "Timur Kasatkin"

deferred class
	NAMED_COMPARABLE_VALUE

inherit

	COMPARABLE

feature {NONE}

	make (s_value: INTEGER; s_name: STRING)
		require
			s_name_exists: s_name /= Void and then not s_name.is_empty
		do
			value := s_value
			name := s_name
			values.force (Current)
		ensure
			value_set: value = s_value
			name_set: name = s_name
		end

feature {NAMED_COMPARABLE_VALUE}

	value: INTEGER

feature --Access

	name: STRING

	values: LINKED_LIST [NAMED_COMPARABLE_VALUE]
		once
			create Result.make
		end

feature -- Comparison

	is_less alias "<" (other: like Current): BOOLEAN
		do
			Result := value < other.value
		end

end
