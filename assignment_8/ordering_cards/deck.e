note
	author: "Timur Kasatkin"

class
	DECK

inherit

	ARRAYED_LIST [CARD]
		redefine
			new_filled_list,
			out
		end

create
	make_full, make_from_array

feature {NONE}

	make_full
		do
			make (52)
			across
				suits as i
			loop
				across
					faces as j
				loop
					force (create {CARD}.make (i.item, j.item))
				end
			end
		end

feature

	suits: LINKED_LIST [CARD_SUIT]
		once
			create Result.make
			suits.extend (create {CARD_SUIT}.make (1, "Clubs"))
			suits.extend (create {CARD_SUIT}.make (2, "Diamonds"))
			suits.extend (create {CARD_SUIT}.make (3, "Hearts"))
			suits.extend (create {CARD_SUIT}.make (4, "Shades"))
		end

	faces: LINKED_LIST [CARD_FACE]
		once
			create Result.make
			faces.extend (create {CARD_FACE}.make (1, "Ace"))
			faces.extend (create {CARD_FACE}.make (2, "two"))
			faces.extend (create {CARD_FACE}.make (3, "three"))
			faces.extend (create {CARD_FACE}.make (4, "four"))
			faces.extend (create {CARD_FACE}.make (5, "five"))
			faces.extend (create {CARD_FACE}.make (6, "six"))
			faces.extend (create {CARD_FACE}.make (7, "seven"))
			faces.extend (create {CARD_FACE}.make (8, "eight"))
			faces.extend (create {CARD_FACE}.make (9, "nine"))
			faces.extend (create {CARD_FACE}.make (10, "ten"))
			faces.extend (create {CARD_FACE}.make (11, "Jack"))
			faces.extend (create {CARD_FACE}.make (12, "Queen"))
			faces.extend (create {CARD_FACE}.make (13, "King"))
		end

feature

	new_filled_list (n: INTEGER): like Current
		do
			Result := Precursor (n)
		end

	out: STRING
		do
			Result := ""
			across
				Current as i
			loop
				Result := Result + "Card " + i.cursor_index.out + ": " + i.item.out + "%N"
			end
		end

invariant
	count <= 52

end
