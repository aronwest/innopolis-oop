#!/usr/bin/env python3
if __name__ == '__main__':
    import os
    import shutil

    for root, dirs, _ in os.walk('/home/timur/dev/eiffel'):
        for dir_ in dirs:
            if 'EIFGENs' == dir_:
                path = os.path.join(root,dir_)
                print('REMOVE: ',path)
                shutil.rmtree(path)
