class
	APPLICATION

inherit

	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
		local
			coffee: CAPPUCCINO
			coffee2: ESPRESSO
			cake: CAKE
			coffee_shop: COFFEE_SHOP
		do
			create coffee.make (10, 8)
			create coffee2.make (12, 8)
			create cake.make (8, 6)
			create coffee_shop.make
			coffee_shop.add_product (coffee)
			coffee_shop.add_product (coffee)
			coffee_shop.add_product (coffee2)
			coffee_shop.add_product (cake)
			coffee_shop.print_menu
			io.new_line
			io.put_string ("Profit: " + coffee_shop.profit.out + "%N")
		end

end
