class
	COFFEE_SHOP

create
	make

feature {NONE}

	make
		do
			create range.make (5)
		end

feature {NONE}

	range: HASH_TABLE [PRODUCT, STRING]

feature

	add_product (product: PRODUCT)
		require
			product_exists: product /= Void
		do
			if range.has_key (product.name) then
				range.found_item.set_amount (range.found_item.amount + 1);
			else
				range.put (product, product.name)
			end
		end

	print_menu
		local
			current_product: PRODUCT
		do
			across
				range.current_keys as a
			loop
				range.search (a.item)
				current_product := range.found_item
				if current_product.amount > 0 then
					io.put_string (current_product.name + " " + current_product.public_price.out)
					io.new_line
				end
			end
		end

	profit: REAL
		local
			sum: REAL
			current_product: PRODUCT
		do
			sum := 0
			across
				range.current_keys as a
			loop
				range.search (a.item)
				current_product := range.found_item
				sum := sum + current_product.amount * current_product.profit
			end
			Result := sum
		end

end
