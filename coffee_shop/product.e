deferred class
	PRODUCT

feature

	make_product (a_public_price: REAL; a_price: REAL; a_name: STRING)
		require
			public_price_more_than_private_price: a_public_price > a_price
			product_name_exists: a_name /= Void
		do
			name := a_name
			price := a_price
			public_price := a_public_price
			description := ""
			amount := 1
		end

feature --Access

	public_price: REAL

	description: STRING

	name: STRING

	amount: INTEGER
			-- Amount of such type of product

feature {NONE}

	price: REAL

feature

	set_amount (an_amount: INTEGER)
		require
			an_amount >= 0
		do
			amount := an_amount
		end

	profit: REAL
		do
			Result := public_price - price
		end

invariant
	public_price > price

end
