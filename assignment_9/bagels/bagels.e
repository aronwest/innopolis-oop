note
	description: "bagels game class"

class
	BAGELS

create
	make

feature {NONE} -- Initialization

	make
			-- Run game.
		local
			answer, guess, number_of_digits: INTEGER
			answer_str, guess_str, result_str: STRING
		do
			io.putstring ("Input n (number of digits): ")
			io.read_integer
			number_of_digits := io.last_integer
			answer := generate_number (number_of_digits)
			answer_str := answer.out
			from
				result_str := ""
				io.putstring ("Try to guess the answer: ")
				io.read_integer
				guess := io.last_integer
				guess_str := guess.out
			until
				guess = answer
			loop
				if guess_str.count /= answer_str.count then
					io.putstring ("Invalid guess: you should enter " + answer_str.count.out + "-digit number. Try once more...%N")
				elseif has_duplicate_digits_or_zero (guess) then
					io.putstring ("Invalid guess: it should not has repeated digits or zeros. Try once more...%N")
				else
					across
						1 |..| number_of_digits as i
					loop
						if answer_str [i.item] = guess_str [i.item] then
							result_str.prepend ("Fermi ")
						else
							if answer_str.has (guess_str [i.item]) then
								result_str.append ("Pico ")
							end
						end
					end
					if result_str.count /= 0 then
						io.putstring (result_str)
					else
						io.putstring ("Bagels")
					end
					io.new_line
				end
				result_str := ""
				io.putstring ("Try to guess the answer: ")
				io.read_integer
				guess := io.last_integer
				guess_str := guess.out
			end
			io.putstring ("You are right!=)")
		end

feature {NONE} --Implementation

	has_duplicate_digits_or_zero (n: INTEGER): BOOLEAN
			-- Check whether `n' has duplicate digits or zeros
		local
			digits: ARRAY [BOOLEAN]
			p: INTEGER
		do
			create digits.make_filled (false, 0, 9)
			from
				Result := false
				p := n
			until
				p = 0 or Result
			loop
				if digits [p \\ 10] or p \\ 10 = 0 then
					Result := true
				else
					digits [p \\ 10] := true
				end
				p := (p / 10).floor
			end
		end

	generate_number (n: INTEGER): INTEGER
			-- Generate random number with `n' digits
		local
			random: V_RANDOM
			min, max: INTEGER
		do
			min := (10 ^ (n - 1)).floor
			max := (10 ^ n).floor - 1
			create random
			from
				Result := random.bounded_item (min, max)
			until
				not has_duplicate_digits_or_zero (Result)
			loop
				random.forth
				Result := random.bounded_item (min, max)
			end
		ensure
			not has_duplicate_digits_or_zero (Result)
		end

end
