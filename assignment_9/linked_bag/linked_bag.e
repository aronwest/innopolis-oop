﻿class
	LINKED_BAG [G]

feature -- Access

	occurrences (v: G): INTEGER
			-- Number of occurrences of `v'.
		local
			c: BAG_CELL [G]
		do
			from
				c := first
			until
				c = Void or else c.value = v
			loop
				c := c.next
			end
			if c /= Void then
				Result := c.count
			end
		ensure
			non_negative_result: Result >= 0
		end

feature -- Element change

	add (v: G; n: INTEGER)
			-- Add `n' copies of `v'.
		require
			n_positive: n > 0
		local
			cur: BAG_CELL [G]
		do
			from
				cur := first
			until
				cur = Void or else cur.value = v
			loop
				cur := cur.next
			end
			if cur /= Void then
				cur.set_count (cur.count + n)
			else
				create cur.make (v)
				cur.set_count (n)
				cur.set_next (first)
				first := cur
			end
		ensure
			n_more: occurrences (v) = old occurrences (v) + n
		end

	remove (v: G; n: INTEGER)
			-- Remove as many copies of `v' as possible, up to `n'.
		require
			n_positive: n > 0
		local
			prev, cur: BAG_CELL [G]
		do
			from
				cur := first
			until
				cur = Void or else cur.value = v
			loop
				prev := cur
				cur := cur.next
			end
			if cur /= Void then
				if cur.count > n then
					cur.set_count (cur.count - n)
				elseif cur = first then
					first := first.next
				else
					prev.set_next (cur.next)
				end
			end
		ensure
			n_less: occurrences (v) = (old occurrences (v) - n).max (0)
		end

	subtract (other: LINKED_BAG [G])
			-- Remove all elements of `other'.
		require
			other_exists: other /= Void
		local
			cur: BAG_CELL [G]
		do
			from
				cur := other.first
			until
				cur = Void
			loop
				remove (cur.value, cur.count)
				cur := cur.next
			end
		end

feature {LINKED_BAG} -- Implementation

	first: BAG_CELL [G]
			-- First cell.

end
