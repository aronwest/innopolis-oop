class
	BUSINESS_CARD

create
	fill_in

feature {NONE} -- Initialization

	fill_in
			-- Fill in the card and print it.
		do
			io.put_string ("Your name: ")
			io.read_line
			set_name (io.last_string)
			io.put_string ("Your job: ")
			io.read_line
			set_job (io.last_string)
			io.put_string ("Your age: ")
			io.read_integer
			set_age (io.last_integer)
			print_card
		end

feature -- Access

	name: STRING
			-- Owner's name.

	job: STRING
			-- Owner's job.

	age: INTEGER
			-- Owner's age.

feature -- Setting

	set_name (a_name: STRING)
			-- Set `name' to `a_name'.
		require
			name_exists: a_name /= Void
			name_not_empty: not a_name.is_empty
		do
			name := a_name.twin
		end

	set_job (a_job: STRING)
			-- Set `job' to `a_job'.
		require
			job_exists: a_job /= Void
		do
			job := a_job.twin
		end

	set_age (a_age: INTEGER)
			-- Set `age' to `a_age'.
		require
			age_non_negative: a_age >= 0
		do
			age := a_age
		end

feature -- Output

	print_card
			-- Print card info to console
		local
			local_width: INTEGER -- used for cases when `name' or `job' length more than `width'
			hor_border: STRING -- up and low border
			name_length: INTEGER
			job_length: INTEGER
		do
			name_length := name.count
			job_length := job.count
			if width < name_length or width < job_length then
				if name_length > job_length then
					local_width := name_length + 5
				else
					local_width := job_length + 5
				end
			else
				local_width := width
			end
			hor_border := line (local_width) + "%N" -- make horizontal border
			io.put_string (hor_border) -- print top border
			io.put_string ("||" + name + spaces (local_width - name.count - 4) + "||%N")
			io.put_string ("||" + age_info + spaces (local_width - age_info.count - 4) + "||%N")
			io.put_string ("||" + job + spaces (local_width - job.count - 4) + "||%N")
			io.put_string (hor_border) -- print bottom border
		end

	age_info: STRING
			-- Text representation of age on the card.
		do
			Result := age.out + " years old"
		end

	Width: INTEGER = 50
			-- Width of the card (in characters), excluding borders.

	line (n: INTEGER): STRING
			-- Horizontal line on length `n'.
		require
			n_positive: n > 0
		do
			Result := "="
			Result.multiply (n)
		end

	spaces (n: INTEGER): STRING
			-- Strign which contains `n' spaces
		require
			n_positive: n > 0
		do
			Result := " "
			Result.multiply (n)
		end

end
