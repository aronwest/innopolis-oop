note
	description: "board_game application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

create
	start_game

feature {NONE} -- Initialization

	start_game
		local
			game: GAME
			players_count: INTEGER
			winner: detachable PLAYER
			c:CHARACTER
		do
			io.putstring (c.out+"%N")
			io.putstring ("Input players count: ")
			io.read_integer

			players_count := io.last_integer
			if 2 <= players_count and players_count <= 6 then
				create game.make (players_count)
				game.play
				winner := game.winner
				if winner /= Void then
					io.putstring ("Player '"
						+ winner.name + "' won. Congratulations!%N")
				end
			else
				io.putstring ("Invalid players count.")
			end
		end

end
