note
	description: "Stores the state of each player in the game and performs a turn."
	author: "Timur Kasatkin"

class
	PLAYER

create
	make

feature {NONE}

	make (player_name: STRING)
		require
			name_exists: player_name /= Void
			name_not_empty: not player_name.is_empty
		do
			name := player_name
			squire_number := 1
		ensure
			name = player_name
		end

feature

	name: STRING

feature {GAME}

	squire_number: INTEGER

	set_squire_number (num: INTEGER)
		require
			1 <= num and num <= 40
		do
			squire_number := num
		ensure
			squire_number = num
		end

	make_turn (die: DIE): PAIR [INTEGER]
		do
			Result := die.gen
		ensure
			result_exists: Result /= Void
		end

invariant
	player_on_cell_between_1_and_40: 1 <= squire_number
										and squire_number <= 40

end
