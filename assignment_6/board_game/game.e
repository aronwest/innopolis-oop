note
	description: "Encapsulates the logic of the game (start state, the structure of a round, ending conditions)."
	author: "Timur Kasatkin"

class
	GAME

inherit

	EXECUTION_ENVIRONMENT

create
	make

feature {NONE}

	make (player_count: INTEGER)
			-- Create game with `player_count' players.
			-- It will ask players to input their names.
		require
			players_count_between_2_and_6: 2 <= player_count
												and player_count <= 6
		local
			player_name: STRING
		do
			winner := Void
			actions_delay := (10 ^ 9).floor
			create die.make
			create players.make (0, player_count - 1)
			across
				0 |..| (player_count - 1) as i
			loop
				from
					player_name := ""
				until
					not player_name.is_empty
				loop
					io.putstring ("Player #" + (i.item + 1).out
						+ " input your name: ")
					io.readline
					player_name := io.last_string
					if player_name.is_empty then
						io.put_string ("Empty name. Try again.%N")
					end
				end
				add_player (player_name.out, i.item)
			end
		ensure
			players.count = player_count
		end

	add_player (player_name: STRING; index: INTEGER)
			-- Add `player' to `players'
			-- into position with index equals to `index'
		local
			new_player: PLAYER
		do
			create new_player.make (player_name)
			players.put (new_player, index)
		end

feature --Access

	winner: detachable PLAYER

	play
			-- Start game and return winner after game ends.
		local
			round_num: INTEGER
		do
			from
				round_num := 1
			until
				winner /= Void
			loop
				play_round (round_num)
				round_num := round_num + 1
			end
		ensure
			winner_exists: winner /= Void
		end

feature {NONE}
	-- Implementation

	border_width: INTEGER = 80

	actions_delay: INTEGER
			--Delay in nanoseconds
			--between actions

	die: DIE
			--Pair of dice

	players: V_ARRAY [PLAYER]

	sum (num_pair: PAIR [INTEGER]): INTEGER
			-- Sum of first and second elements of `num_pair'
		require
			num_pair_exists: num_pair /= Void
		do
			Result := num_pair.first + num_pair.second
		end

	multiplied_str (str: STRING; n: INTEGER): STRING
			-- Return new string which is `str' multiplied by `n'
		require
			str_not_empty: not str.is_empty
			n_positive: n >= 1
		do
			Result := str
			Result.multiply (n)
		ensure
			result_exists: Result /= Void
		end

	make_turn_by (player: PLAYER)
			-- Make turn by `player' and change his position
		local
			roll: PAIR [INTEGER]
			new_square: INTEGER
			border: STRING
		do
			roll := player.make_turn (die)
			border := multiplied_str ("=", border_width) + "%N"
			sleep ((actions_delay / 3).floor)
			io.putstring (border)
			io.putstring ("Player '" + player.name
				+ "' took turn " + roll.out + ". ")
			if roll.first ~ roll.second then
				if player.squire_number - sum (roll) <= 1 then
					new_square := 1
				else
					new_square := player.squire_number - sum (roll)
				end
			else
				if player.squire_number + sum (roll) > 40 then
					new_square := 0
				else
					new_square := player.squire_number + sum (roll)
				end
			end
			if new_square /~ 0 then
				io.putstring ("Player '" + player.name + "' moves ")
				if new_square > player.squire_number then
					io.putstring ("forward")
				else
					io.putstring ("back")
				end
				player.set_squire_number (new_square)
				io.putstring (" on squire "
					+ player.squire_number.out + "%N")
			else
				io.putstring ("Player '" + player.name
					+ "' took too much. He stays in place.%N")
			end
			if player.squire_number ~ 40 then
				winner := player
			end
			io.putstring (border)
			sleep ((actions_delay / 3).floor)
		end

	play_round (round_num: INTEGER)
			-- Play round with number `round_num'
		require
			round_num >= 1
		local
			cur: like players.new_cursor
			cur_player: PLAYER
			border, border_half: STRING
		do
			border_half := multiplied_str ("|",
				((border_width - 7) / 2).floor)
			border := border_half + "ROUND #"
				+ round_num.out + border_half + "%N"
			sleep ((actions_delay / 2).floor)
			io.putstring (border)
			from
				cur := players.new_cursor
			until
				cur.after or winner /= Void
			loop
				cur_player := cur.item
				make_turn_by (cur_player)
				cur.forth
			end
			border_half := multiplied_str ("|",
				((border_width - 7) / 2).floor - 2)
			border := border_half + "ROUND #"
				+ round_num.out + " END" + border_half + "%N%N"
			io.putstring (border)
			sleep ((actions_delay / 2).floor)
		end

invariant
	players_count_between_2_and_6: 2 <= players.count
									and players.count <= 6

end
