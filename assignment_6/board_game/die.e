note
	description: "Pair of dice."
	author: "Timur Kasatkin"

class
	DIE

create
	make

feature {NONE}

	random: V_RANDOM

	make
		do
			create random.default_create
		end

feature -- Generation

	gen: PAIR[INTEGER]
		local
			pair:PAIR[INTEGER]
			first,second:INTEGER
		do
			first:=random.bounded_item (1, 6)
			random.forth
			second:=random.bounded_item (1, 6)
			random.forth
			create pair.make(first,second)
			Result := pair
		end

end
