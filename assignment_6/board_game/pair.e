note
	description: "Summary description for {PAIR}."
	author: "Timur Kasatkin"

class
	PAIR [G]

inherit

	ANY
		redefine
			out
		end

create
	make

feature {NONE} --Initialization

	make (first_val, second_val: attached G)
		require
			first_exists: first_val /= Void
			second_exists: second_val /= Void
		do
			first := first_val
			second := second_val
		ensure
			first = first_val
			second = second_val
		end

feature

	first: attached G

	second: attached G

	out: STRING
		do
			Result := "[" + first.out + "," + second.out + "]"
		end

invariant
	first_exists: first /= Void
	second_exists: second /= Void

end
