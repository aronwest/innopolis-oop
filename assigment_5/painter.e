note
	author: "Timur Kasatkin"
class
	PAINTER

create
	fill_dimension

feature {NONE}

	fill_dimension
		local
			dimension: INTEGER
		do
			io.put_string ("Enter the dimension: ")
			io.read_integer
			dimension := io.last_integer
			if dimension < 2 then
				io.putstring ("Invalid dimension. ")
				io.putstring ("It must be number "
					+"which not less than 2. ")
			else
				paint (dimension)
			end
		end

feature

	paint (dimension: INTEGER)
			-- Paint rectangle and its mirror
			-- with dimension equals to `dimension'.
		require
				-- it make no sence to print rectanlge
				-- with dimension less than 2.
			dimension_not_less_then_two: dimension >= 2
		local
			cell_w, line_width: INTEGER
			switch: BOOLEAN
			picture, line: STRING
		do
			io.put_string ("Triangle and its mirror:%N")
			line_width := dimension * 2 + 1
			across
				1 |..| dimension as i
			from
				switch := true
				cell_w := 1
				picture := ""
			loop
				line := ""
				across
					1 |..| cell_w as j
				loop
					if switch then
						line.append ("*")
					else
						line.append (" ")
					end
					switch := not switch
				end
				if i.item \\ 2 = 0 then
						-- if current line number even
						-- change `switch' to true
						-- to print asterisk on next line.
					switch := true
				end
				line := line
					+ spaces (line_width - cell_w * 2)
					+ line.mirrored
				picture.append (line + "%N")
				cell_w := cell_w + 1
			end
			io.putstring (picture)
		end

	spaces (count: INTEGER): STRING
			-- String containing `count' spaces
		require
			count_positive: count > 0
		do
			Result := " "
			Result.multiply (count)
		end

end
