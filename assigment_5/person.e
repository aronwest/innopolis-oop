note
	description: "Summary description for {PERSON}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PERSON

create
	make

feature -- Initialization

	make (s: STRING)
			--Set `name' to `s'
		require
			s_non_empty: s /= Void and then not s.is_empty
		do
			name := s
		ensure
			name_set: name = s
		end

feature --Access

	name: STRING
			-- Person's name.

	loved_one: PERSON
			-- Person's loved one.

feature --Basic operations

	set_loved_one (p: PERSON)
			-- Set `loved_one' to `p'.
		do
			loved_one := p
		ensure
			loved_one_set: loved_one = p
		end

invariant
	has_name: name /= Void and then not name.is_empty

end
