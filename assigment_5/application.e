note
	description: "assigment_5 application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit

	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
		local
			i, j: INTEGER
			x, y, z: PERSON
		do
			create x.make ("Anna")
			create y.make ("Ben")
			create z.make ("Chloe")
			x.set_loved_one (y)
			y.set_loved_one (z)

				--Num 8
				--y:=x.loved_one -- y is attached to 'Ben'
				--x.set_loved_one (z) -- x (which is attached to 'Anna') loves 'Chloe'
				--z:=y -- z attached to 'Ben'; 'Anna' loves 'Ben' now

			--Num9
			z:=x.loved_one -- z is attached to 'Ben'
			z.set_loved_one (x) -- z ('Ben') loves 'Anna'
			y:=y.loved_one.loved_one 

			io.putstring ("x is: " + x.name.out + "%N")
			io.putstring ("y is: " + y.name.out + "%N")
			io.putstring ("z is: " + z.name.out + "%N")
			io.putstring ("x loves: " + x.loved_one.name.out + "%N")
			io.putstring ("y loves: " + y.loved_one.name.out + "%N")
			io.putstring ("z loves: " + z.loved_one.name.out + "%N")
				--Here the code snippets from below are added
		end

end
