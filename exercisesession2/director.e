note
	description : "exercisesession2 application root class"
	date        : "$Date$"
	revision    : "$Revision$"

class
	DIRECTOR

inherit
	ARGUMENTS

create
	feature_and_play

feature {NONE} -- Initialization

	feature_and_play
		local
			acrobat1,acrobat2,acrobat3:ACROBAT
			partner1,partner2:ACROBAT_WITH_BUDDY
			author1:AUTHOR
			curmudgeon1:CURMUDGEON
		do
			create acrobat1
			create acrobat2
			create acrobat3
			create partner1.make(acrobat1)
			create partner2.make(acrobat2)
			create author1
			create curmudgeon1
			author1.clap (4)
			partner1.twirl (4)
			curmudgeon1.clap (7)
			acrobat2.clap (curmudgeon1.count)
			acrobat3.twirl (partner2.count)
			partner1.buddy.clap (partner1.count)
			partner2.clap (2)
		end

end
