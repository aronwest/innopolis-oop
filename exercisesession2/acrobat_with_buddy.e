note
	description: "Summary description for {ACROBAT_WITH_BUDDY}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	ACROBAT_WITH_BUDDY

inherit
	ACROBAT
	redefine
		twirl,clap,count
	end

create
	make

feature
	buddy:ACROBAT

feature
	make(p:ACROBAT)
		do
			buddy:=p
		end

	clap (num: INTEGER_32)
	do
		
	end

	twirl (num: INTEGER_32)
	do

	end

	count:INTEGER
	do

	end

end
