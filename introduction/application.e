class
	APPLICATION

create
	execute

feature {NONE} -- Initialization

	execute
			-- Run application.
		do
			io.put_string ("Name: Timur Kasatkin")
			io.new_line
			io.put_string ("Age: 20")
			io.new_line
			io.put_string ("Mother tongue: RUSSIAN")
			io.new_line
			io.put_string ("Has a cat: False")
			io.new_line
		end

end
