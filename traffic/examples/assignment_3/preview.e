note
	description: "Introduction to Traffic."

class
	PREVIEW

inherit

	ZURICH_OBJECTS

feature -- Explore Zurich

	explore
			-- Modify the map.
		do

				-- What is the name of kind of transport which goes on line number 5?
			console.output ("2.1: " + zurich.line (5).kind.name.generating_type.out)

				-- What is the distance between zero coordinate of the Zurich map and coordinates of "Hardplatz" station?
			wait (1)
			console.output ("2.2: " + Zurich.station ("Hardplatz").position.length.generating_type.out)

				-- What is the distance between station "Bellevue" and terminal station at the west end of the second line along line 2?
			wait (1)
			console.output ("2.3: " + Zurich.line (2).distance (Zurich.station ("Bellevue"), Zurich.line (2).west_terminal).generating_type.out)



				-- How bright is the color of line 13?
			wait (1)
			console.output ("3.1: " + zurich.line (13).color.brightness.out)

				-- How many meters to the north of the city center is the third station of line 31 located?
			wait (1)
			console.output ("3.2: " + zurich.line (31).stations[3].position.y.minus(zurich.station ("Center").position.y).abs.out)

				-- What is the next station of line 31 after Loewenplatz in the direction of its west terminal?
			wait (1)
			console.output ("3.3: " + zurich.line (31).next_station (zurich.station ("Loewenplatz"), zurich.line (31).west_terminal).out)

				-- (Optional) How many public transportation lines go through station Paradeplatz?
			wait (1)
			console.output ("3.4: " + zurich.station ("Paradeplatz").lines.count.out)

				-- (Optional) Does line 7 connect stations Paradeplatz and Rennweg directly?
			wait (1)
			console.output ("3.5: " + zurich.line (7).next_station (zurich.station ("Paradeplatz"),
				zurich.line (7).direction (zurich.station ("Paradeplatz"),zurich.station ("Rennweg")))
							.name.is_equal("Rennweg").out)
		end

end
