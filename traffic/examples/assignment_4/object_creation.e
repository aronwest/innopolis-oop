note
	description: "Creating new objects for Zurich."

class
	OBJECT_CREATION

inherit

	ZURICH_OBJECTS

feature -- Explore Zurich

	explore
			-- Move "Central".
		local
				--			i: like zurich.stations.new_cursor
			station: STATION
		do
			across
				zurich.stations as i
			loop
				if i.item.name ~ "Central" then
					station := i.item
				end
				if station /= Void then
					station.set_position ([0.0, 0.0])
				end
			end
				--			from
				--				i := zurich.stations.new_cursor
				--			until
				--				i.after or else i.item.name ~ "Central"
				--			loop
				--				i.forth
				--			end
				--			if not i.after then
				--				i.item.set_position ([0.0, 0.0])
				--			end
				--			add_buildings ("ETH main building", 400, 400, 350, 350)
				--			add_buildings ("Confiserie Sprugli", 550, 550, 600, 600)
				--			add_route
		end

	add_buildings (new_building_address: STRING; corner1_x, corner1_y, corner2_x, corner2_y: INTEGER)
			-- Add new building with specified address and corners.
		local
			new_building: BUILDING
			corner1, corner2: VECTOR
		do
			create corner1.make (corner1_x, corner1_y)
			create corner2.make (corner2_x, corner2_y)
			create new_building.make (new_building_address, corner1, corner2)
			zurich.add_building (new_building)
		end

	add_route
			-- Create a route starting at station Polyterrasse,
			-- passing through Paradeplatz and ending at Opernhaus.
		local
			leg1, leg2, leg3: LEG
			new_route: ROUTE
		do
			create leg1.make (zurich.station ("Polyterrasse"), zurich.station ("Central"), zurich.line (24))
			create leg2.make (zurich.station ("Central"), zurich.station ("Paradeplatz"), zurich.line (7))
			create leg3.make (zurich.station ("Paradeplatz"), zurich.station ("Opernhaus"), zurich.line (2))
			leg1.link (leg2)
			leg2.link (leg3)
			create new_route.make (leg1)
			zurich.add_route (new_route)
		end

end
