note
	description: "Introduction to Traffic."

class
	PREVIEW

inherit
	ZURICH_OBJECTS

feature -- Explore Zurich

	explore
			-- Modify the map.
		local
			i:INTEGER
			new_station_view:STATION_VIEW
		do
			zurich.add_station ("Zoo", 1100,-500)
			zurich.connect_station (6, "Zoo")
			zurich_map.update
			zurich_map.fit_to_window
			new_station_view:=zurich_map
					.station_view (zurich.station ("Zoo"))
			from i:=1
			until i>10
			loop
				new_station_view.highlight
				wait (1)
				new_station_view.unhighlight
				wait (1)
				i:=i+1
			end
		end

end
