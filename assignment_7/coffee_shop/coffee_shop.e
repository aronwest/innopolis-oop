class
	COFFEE_SHOP

create
	make

feature --Menu

	range: HASH_TABLE [PRODUCT, STRING]

feature

	make
		do
			create range.make (5)
		end

feature

	add_product (product: PRODUCT)
		local
			a_name: STRING
		do
			a_name := product.name
			if range.has_key (a_name) then
				range.found_item.set_amount (range.found_item.amount + 1);
			else
				range.put (product, product.name)
			end
		end

	print_menu
		local
			products: ARRAY [STRING]
			current_key: STRING
			current_product: PRODUCT
		do
			products := range.current_keys
			across
				products as a
			loop
				current_key := a.item
				range.search (current_key)
				current_product := range.found_item
				if current_product.amount > 0 then
					io.put_string (current_product.name + " " + current_product.public_price.out)
					io.new_line
				end
			end
		end

end
