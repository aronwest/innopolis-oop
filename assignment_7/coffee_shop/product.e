deferred class
	PRODUCT

feature

	make_product (a_public_price: REAL; a_price: REAL; a_name: STRING)
		require
			a_public_price > a_price
			a_name /= Void
		do
			name := a_name
			price := a_price
			public_price := a_public_price
			description := ""
			amount := 1
		ensure
			public_price > price
		end

feature --Access

	public_price: REAL

	price: REAL

	description: STRING

	name: STRING

	amount: INTEGER

feature

	set_amount (an_amount: INTEGER)
		do
			amount := an_amount
		end

	set_name (a_name: STRING)
		do
			name := a_name
		end

	set_description (a_description: STRING)
		do
			description := a_description
		end

	set_price (a_price: REAL)
		do
			price := a_price
		end

	set_price_public (a_public_price: REAL)
		do
			public_price := a_public_price
		ensure
			price < public_price
		end

	profit: REAL
		do
			Result := public_price
		end

end
