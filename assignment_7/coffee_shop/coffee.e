class
	COFFEE

inherit

	PRODUCT

create
	make

feature

	make (a_public_price, a_price: REAL)
		do
			make_product (a_public_price, a_price, "COFFEE")
		end

end
