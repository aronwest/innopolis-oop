class
	APPLICATION

inherit

	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
		local
			coffee: CAPPUCCINO
			coffee_shop: COFFEE_SHOP
		do
			create coffee.make (10, 8)
			create coffee_shop.make
			coffee_shop.add_product (coffee)
			coffee_shop.print_menu
		end

end
