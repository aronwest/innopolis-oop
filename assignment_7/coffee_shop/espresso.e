class
	ESPRESSO

inherit

	COFFEE
		redefine
			make
		end

create
	make

feature

	make (a_public_price, a_price: REAL)
		do
			make_product (a_public_price, a_price, "ESPRESSO")
		end

end
