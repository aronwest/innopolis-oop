note
	description: "board_game application root class"

class
	APPLICATION

create
	start_game

feature {NONE} -- Initialization

	start_game
		local
			game: GAME_V2
			players_count: INTEGER
--			winner: detachable PLAYER
			c: CHARACTER
			winners: detachable LIST [PLAYER]
		do
			io.putstring (c.out + "%N")
			io.putstring ("Input players count: ")
			io.read_integer
			players_count := io.last_integer
			if 2 <= players_count and players_count <= 6 then
				create game.make (players_count)
				game.play
				winners := game.winners
				if game.winner /= Void then
					if winners /= Void then
						io.put_string ("There are several winners: ")
						across winners as i
						loop
							io.put_string ("	"+i.item.name+"%N")
						end
						io.put_string ("Congratulation!!!%N")
					else
						io.putstring ("Player '" + game.winner.name + "' won. Congratulations!%N")
					end
				end
			else
				io.putstring ("Invalid players count.")
			end
		end

end
