note
	description: "Summary description for {LOTTERY_SQUARE}."

class
	LOTTERY_SQUARE

inherit

	SQUARE
		redefine
			place_player
		end

create
	make

feature

	money_to_add: INTEGER = 70

	place_player (player: PLAYER)
		do
			Precursor(player)
			if attached {PLAYER_WITH_MONEY} player as player_with_money then
				player_with_money.add_money (money_to_add)
			end
		end

end
