note
	description: "Summary description for {GAME_V2}."

class
	GAME_V2

inherit

	GAME
		redefine
			make,
			make_turn_by
		end

create
	make

feature {NONE} --Initialiation

	make (player_count: INTEGER)
		local
			i: INTEGER
		do
			Precursor (player_count)
			from
				i := 0
			until
				i >= players.count
			loop
				players.put (create {PLAYER_WITH_MONEY}.make (players [i].name), i)
				i := i + 1
			end
			create squares.make (1, 40)
			from
				i := 1
			until
				i > squares.count
			loop
				if i \\ 5 = 0 then
					if i \\ 10 = 0 then
						squares.put (create {LOTTERY_SQUARE}.make, i)
					else
						squares.put (create {BAD_INVESTMENT_SQUARE}.make, i)
					end
				else
					squares.put (create {SQUARE}.make, i)
				end
				i := i + 1
			end
			across
				players as c
			loop
				squares [1].place_player (c.item)
			end
		end

feature {NONE} -- Implementation

	squares: V_ARRAY [SQUARE]

	make_turn_by (player: PLAYER)
		local
			new_square_num: INTEGER
			border: STRING
			old_square: SQUARE
		do
			border := multiplied_str ("=", border_width) + "%N"
			sleep ((actions_delay / 3).floor)
			io.putstring (border)
			new_square_num := gen_new_squire_num (player)
			if new_square_num /~ 0 then
				io.putstring ("Player '" + player.name + "' moves ")
				if new_square_num > player.squire_number then
					io.putstring ("forward")
				else
					io.putstring ("back")
				end
				old_square := squares [player.squire_number]
				old_square.remove_player (player)
				player.set_squire_number (new_square_num)
				squares [player.squire_number].place_player (player)
				if attached {PLAYER_WITH_MONEY} player as player_with_money then
					io.putstring (" on squire " + player.squire_number.out + ". Now he has " + player_with_money.money.out + " rubles." + "%N")
				end
			else
				io.putstring ("Player '" + player.name + "' took too much. He stays in " + player.squire_number.out + ".%N")
			end
			if player.squire_number ~ 40 then
				detect_winners
			end
			io.putstring (border)
			sleep ((actions_delay / 3).floor)
		end

	detect_winners
		local
			max_money: INTEGER
		do
			across
				players as i
			loop
				if attached {PLAYER_WITH_MONEY} i.item as player_with_money and then player_with_money.money > max_money then
					max_money := player_with_money.money
				end
			end
			winners := create {LINKED_LIST [PLAYER]}.make
			across
				players as i
			loop
				if attached {PLAYER_WITH_MONEY} i.item as player_with_money and then player_with_money.money = max_money then
					winners.force (player_with_money)
				end
			end
			winner := winners.first
			if winners.count = 1 then
				winners := Void
			end
		end

feature

	winners: detachable LIST [PLAYER]
			-- Winners

invariant
	winners_not_void_if_there_several_winners: winners /= Void implies winners.count > 1

end
