note
	description: "Summary description for {SQUARE}."

class
	SQUARE

create
	make

feature {NONE} --Initializaton

	make
		do
			players := create {ARRAYED_LIST [PLAYER]}.make (6)
		ensure
			players /= Void
		end

feature --Access

	players: LIST [PLAYER]

	place_player (player: PLAYER)
		require
			player_exists: player /= Void
			player_not_already_on_square: not players.has (player)
		do
			players.force (player)
		ensure
			players.has (player)
		end

	remove_player (player: PLAYER)
		require
			player_exists: player /= Void
			player_on_square: players.has (player)
		do
			players.prune (player)
		ensure
			not players.has (player)
		end

end
