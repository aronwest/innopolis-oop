note
	description: "Summary description for {PLAYER_WITH_MONEY}."

class
	PLAYER_WITH_MONEY

inherit

	PLAYER
		redefine
			make
		end

create
	make

feature {NONE} --Initialization

	make (player_name: STRING)
		do
			Precursor (player_name)
			money := start_money
		ensure then
			money = start_money
		end

feature --Access

	start_money:INTEGER = 50

	money: INTEGER

	add_money (count: INTEGER)
		require
			count_positive: count > 0
		do
			money := money + count
		ensure
			money = old money + count
		end

	withdraw_money (count: INTEGER)
		require
			count_positive: count > 0
		do
			if count <= money then
				money := money - count
			else
				money := 0
			end
		ensure
			count <= old money implies money = old money - count
			count > old money implies money = 0
		end

invariant
	money_not_negative: money >= 0

end
