note
	description: "Summary description for {BAD_INVESTMENT_SQUARE}."

class
	BAD_INVESTMENT_SQUARE

inherit

	SQUARE
		redefine
			place_player
		end

create
	make

feature

	money_to_del: INTEGER = 30

	place_player (player: PLAYER)
		do
			Precursor(player)
			if attached {PLAYER_WITH_MONEY} player as player_with_money then
				player_with_money.withdraw_money (money_to_del)
			end
		end

end
