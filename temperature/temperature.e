note
	description: "Temperature."

class
	TEMPERATURE

create
	make_celsius, make_kelvin

feature -- Initialization

	make_celsius (v: INTEGER)
			-- Create with Celsius value `v'.
		require
			higher_or_equals_than_minus_273: v >= -273
		do
				-- Create a temperature object
				-- encapsulating value 'v' intended in Celsius.
			celsius := v
		end

	make_kelvin (v: INTEGER)
			-- Create with Kelvin value `v'.
		require
			not_below_zero: v >= 0
		do
				-- Create a temperature object
				-- encapsulating value 'v' intended in Kelvin.
			celsius := v - 273
		end

feature -- Access

	celsius: INTEGER
			-- Value in Celsius scale.

	kelvin: INTEGER
			-- Value in Kelvin scale.
		do
				-- Compute the Kelvin temperature
				-- value from the Celsius value
				Result:=celsius+273
		end

feature -- Measurement

	average (other: TEMPERATURE): TEMPERATURE
			-- Average temperature between `Current' and `other'.
		require
			other_exists: other /= Void
		local
			average_temperature: TEMPERATURE
		do
				-- Compute the average of two temperature.
				-- One is given by the current object,
				-- the other is passed as an argument.
			create average_temperature
				.make_celsius (((celsius + other.celsius) / 2).floor)
			Result := average_temperature
		end

invariant
	kelvin_not_below_zero: kelvin >= 0

end
