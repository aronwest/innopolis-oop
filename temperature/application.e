note
	description: "Temperature application root class"

class
	APPLICATION

create
	make

feature {NONE} -- Initialization

	make
		local
			temp_1, temp_2, average_temp: TEMPERATURE
			i: INTEGER
		do
				-- Input temperature in Celsius
				-- and show the converted value in Kelvin.
			io.put_string ("Enter the first temperature in Celsius: ")
			io.read_integer
			i := io.last_integer
			create temp_1.make_celsius (i)
			io.put_string ("The first temperature in Kelvin is: "
				+ temp_1.kelvin.out + "%N")

				-- Input temperature in Kelvin
				-- and show the converted value in Celsius.
			io.put_string ("Enter the second temperature in Kelvin: ")
			io.read_integer
			i := io.last_integer
			create temp_2.make_kelvin (i)
			io.put_string ("The second temperature in Celsius is: "
				+ temp_2.celsius.out + "%N")

				-- Calculate the average temperature
				-- and show it in both Celsius and Kelvin.
			average_temp := temp_1.average (temp_2)
			io.put_string ("The average in Celsius is: "
				+ average_temp.celsius.out+"%N")
			io.put_string ("The average in Kelvin is: "
				+ average_temp.kelvin.out)
		end

end
