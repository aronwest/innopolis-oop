note
	description: "project-bubble-agent application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit

	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
		local
			un_sorted_array: ARRAY [INTEGER]
			sorted_array: ARRAY [INTEGER]
			sorter: SORTING_ALGORITHMS [INTEGER]
			sort_agent: FUNCTION [COMPR, TUPLE [INTEGER, INTEGER], INTEGER]
			compr:COMPR
		do
			create un_sorted_array.make (1, 8)
			create sorter
			create compr

			sort_agent := agent compr.int_compare (?, ?)
			un_sorted_array [1] := 6
			un_sorted_array [2] := 5
			un_sorted_array [3] := 3
			un_sorted_array [4] := 1
			un_sorted_array [5] := 8
			un_sorted_array [6] := 7
			un_sorted_array [7] := 2
			un_sorted_array [8] := 4

			sorted_array := sorter.bubble_sort (un_sorted_array, sort_agent)

			across
				sorted_array.lower |..| sorted_array.upper as iter
			loop
				print (sorted_array [iter.item].out +"%N")
			end

		end

end
