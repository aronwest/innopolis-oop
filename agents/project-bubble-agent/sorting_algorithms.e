note
	description: "Summary description for {SORTING_ALGORITHMS}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	SORTING_ALGORITHMS [G]
feature
	bubble_sort (input: ARRAY [G]; comp: FUNCTION [COMPR, TUPLE [G, G], INTEGER]): ARRAY [G]
		local
			l_unchanged: BOOLEAN
			l_item_count: INTEGER
			l_temp: G
		do
			create Result.make (input.lower, input.upper)
			Result.copy (input)
			from
				l_item_count := Result.count
			until
				l_unchanged
			loop
				l_unchanged := True
				l_item_count := l_item_count - 1
				across
					1 |..| l_item_count as ic
				loop
					if  comp.item ([Result [ic.item], Result [ic.item + 1]]) = 1 then
						l_temp := Result [ic.item]
						Result [ic.item] := Result [ic.item + 1]
						Result [ic.item + 1] := l_temp
						l_unchanged := False
					end
				end
			end
		end

end
