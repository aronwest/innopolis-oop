note
	description: "Summary description for {INT_COMPR}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	COMPR
feature

	int_compare (n, m: INTEGER): INTEGER
		do
			if n > m then
				Result := 1
			elseif n < m then
				Result := -1
			else
				Result := 0
			end
		end

	double_compare (n, m: DOUBLE): INTEGER
		do
			if n > m then
				Result := 1
			elseif n < m then
				Result := -1
			else
				Result := 0
			end
		end

end
