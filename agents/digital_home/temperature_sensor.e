note
	description: "Summary description for {TEMPERATURE_SENSOR}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	TEMPERATURE_SENSOR

create
	make

feature

	temperature: DOUBLE

	reactions: V_HASH_SET [PROCEDURE [ANY, TUPLE [DOUBLE]]]

	make
		do
			create reactions
		end

	set_temperature (a_temperature: DOUBLE)
		do
			temperature := a_temperature
			across
				reactions as react_iter
			loop
				react_iter.item (a_temperature)
			end
		end

	add_reaction (a_reaction: PROCEDURE [ANY, TUPLE [DOUBLE]])
		do
			reactions.extend (a_reaction)
		end

	remove_reaction (a_reaction: PROCEDURE [ANY, TUPLE [DOUBLE]])
		do
			reactions.remove (a_reaction)
		end

end
