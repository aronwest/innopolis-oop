class
	APPLICATION

create
	make

feature {NONE} -- Initialization

	make
		local
			s: TEMPERATURE_SENSOR
			d: DISPLAY
			c: HEATING_CONTROLLER
		do
			create s.make
			create d
			create c.set_goal (21.5)
			s.add_reaction (agent d.show(?))
			s.add_reaction (agent c.adjust(?))
			s.set_temperature (22)
			s.set_temperature (22.8)
			s.set_temperature (20.0)
		end

end
