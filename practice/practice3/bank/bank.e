note
	description : "bank application root class"
	date        : "$Date$"
	revision    : "$Revision$"

class
	BANK

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
			acc1,acc2:ACCOUNT
		do
			create acc1.make
			create acc2.make
			acc1.deposit (15)
			acc1.set_credit_limit (60)
			print("Acc1 available: ")
			print(acc1.available_amount)
			print("%N")
--			acc1.withdraw (80)
--			acc1.deposit (-5)
			print("Transfer 50$ from acc1 to acc2%N")
			acc1.transfer (50, acc2)
			print("Acc1 available: ")
			print(acc1.available_amount)
			print("%N")
			print("Acc2 available: ")
			print(acc2.available_amount)
			print("%N")
			print("Acc1 balance: ")
			print(acc1.balance)
			print("%N")
			print("Acc1 credit limit: ")
			print(acc1.credit_limit)
			print("%N")
			print("Acc1 available: ")
			print(acc1.available_amount)
			print("%N")
			print("withdraw 20$ from acc1%N")
			acc1.withdraw (20)
			print("Acc1 balance: ")
			print(acc1.balance)
			print("%N")
			print("Acc1 credit limit: ")
			print(acc1.credit_limit)
			print("%N")
			print("Acc1 available: ")
			print(acc1.available_amount)
			print("%N")
			acc1.withdraw (6)
			--acc1.transfer (100, acc1)
		end

end
