note
	description: "Account class with balance and credit limit"
	author: "Timur Kasatkin"

class
	ACCOUNT

create
	make

feature{NONE}
	make
		do
			balance:=0
			credit_limit:=0
		end

feature --Access

	balance: INTEGER
					-- Balance of this account.					

	credit_limit: INTEGER

	available_amount: INTEGER
			do
				Result:= balance + credit_limit
			ensure
				available_not_negative:Result>=0
			end

feature
	set_credit_limit(limit:INTEGER)
			require
				limit_not_negative:limit>=0
				balance_bigger_than_limit:balance>=-limit
			do
				credit_limit:=limit
			ensure
				credit_changed:credit_limit=limit
			end

	deposit (amount:INTEGER)
			require
				amount_not_negative:amount>0
			do
				balance:= balance+amount
			ensure
				balance=old balance+amount
			end

	withdraw(amount:INTEGER)
			require
				amount_not_negative:amount>0
				amount_not_bigger_than_availabe:amount<=balance+credit_limit
			do
				balance:=balance-amount
			ensure
				balance=old balance-amount
			end

feature
	transfer (amount:INTEGER; other:ACCOUNT)
			require
				other_acc_is_not_you:other/=Current
				amount_not_negative:amount>0
				amount_not_bigger_than_availabe:amount<=balance+credit_limit
			do
				balance:=balance-amount
				other.deposit (amount)
			ensure
				balance = old balance - amount
				other.balance = old other.balance + amount
			end

invariant
	credit_limit_not_negative:0<=credit_limit
	balance_not_negative:-credit_limit<=balance

end
