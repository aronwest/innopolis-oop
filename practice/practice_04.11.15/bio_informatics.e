note
	description: "Summary description for {BIO_INFORMATICS}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	BIO_INFORMATICS

inherit

	COMPUTER_SCIENTIST
		undefine
			introduce,
			discipline
		end

	BIOLOGIST
		rename
			introduce as B_introduce
		redefine
			discipline
		select
			B_introduce
		end

create
	introduce

feature {NONE}

	introduce (b_i_id: INTEGER; b_i_name, b_i_biography: STRING; b_i_pet: PET)
		require
			b_i_name /= Void
			b_i_biography /= Void
			b_i_pet /= Void
		do
			B_intoduce (b_i_id, b_i_name, b_i_pet)
			biography := b_i_biography
		end

feature

	biography: STRING

	discipline: STRING
		do
			Result := "Bio-informatics"
		end

end
