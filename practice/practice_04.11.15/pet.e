note
	description: "Summary description for {PET}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PET

create
	grow

feature {NONE}

	grow (p_name: STRING)
		require
			p_name_exists: p_name /= Void
		do
			name := p_name
		ensure
			name = p_name
		end

feature

	name: STRING

invariant
	name_exists: name /= Void

end
