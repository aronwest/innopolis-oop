note
	description: "Summary description for {BIOLOGIST}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	BIOLOGIST

inherit

	SCIENTIST
	rename
		introduce as S_introduce
	end

create
	introduce

feature {NONE}

	introduce (b_id: INTEGER; b_name: STRING; b_pet: PET)
		require
			b_name /= Void
			b_pet /= Void
		do
			S_introduce (b_id, b_name)
			pet := b_pet
		ensure
			name = b_name
			pet = b_pet
		end

feature

	pet: PET

	discipline: STRING
		do
			Result := "Biologist"
		end

invariant
	pet_exists:pet /=Void

end
