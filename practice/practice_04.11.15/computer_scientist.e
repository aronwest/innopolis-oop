note
	description: "Summary description for {COMPUTER_SCIENTIST}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	COMPUTER_SCIENTIST

inherit

	SCIENTIST

create
	introduce

feature

	discipline: STRING
		do
			Result := "Computer scientist"
		end

end
