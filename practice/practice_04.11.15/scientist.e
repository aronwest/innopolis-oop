note
	description: "Summary description for {SCIENTIST}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

deferred class
	SCIENTIST

feature --Initialization

	introduce (s_id: INTEGER; s_name: STRING)
		require
			s_name /= Void
		do
			id := s_id
			name := s_name
		ensure
			id = s_id
			name = s_name
		end

feature

	id: INTEGER

	name: STRING

	discipline: STRING
		deferred
		ensure
			Result /= Void
		end

invariant
	name /= Void

end
