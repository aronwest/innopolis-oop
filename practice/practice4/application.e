note
	description : "practice4 application root class"
	date        : "$Date$"
	revision    : "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
			clock1:CLOCK
			i:INTEGER
		do
			create clock1.make
			io.put_string (clock1.to_str)
			io.new_line
			from
				i:=0
			until
				i>25
			loop
				clock1.increment_hour
				i:=i+1
			end
			io.put_string (clock1.to_str)
			io.new_line
		end

end
