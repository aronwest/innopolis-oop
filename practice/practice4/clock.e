note
	description: "Summary description for {CLOCK}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CLOCK

create
	make,with

feature {NONE} -- Initialization

	make
			-- Initialization for `Current'.
		do
			hour:=0
			min:=0
			sec:=0
		end

	with(hours,minutes,seconds:INTEGER)
		require
			hours_between_0_and_23:0<=hours AND hours<=23
			minutes_between_0_and_59:0<=minutes AND minutes<=59
			seconds_between_0_and_59:0<=seconds AND seconds<=59
		do
			hour:=hour
			min:=minutes
			sec:=seconds
		end

feature --ATTRIBUTES		

	hour:INTEGER

	min:INTEGER

	sec:INTEGER

feature --SETTERS

	set_hour(new_hour:INTEGER)
		require
			new_hour_between_0_and_23:0<=new_hour AND new_hour<=23
		do
			hour := new_hour
		ensure
			hour = new_hour
		end

	set_min(new_min:INTEGER)
		require
			new_min_between_0_and_59:0<=new_min AND new_min<=59
		do
			min := new_min
		ensure
			min = new_min
		end

	set_sec(new_sec:INTEGER)
		require
			new_sec_between_0_and_59:0<=new_sec AND new_sec<=59
		do
			sec := new_sec
		ensure
			sec = new_sec
		end

feature --INCREMENT

	increment_hour
		do
			hour:=(hour + 1) \\ 24
		ensure
			hour = (old hour + 1) \\ 24
		end

	increment_min
		do
			min:=(min + 1) \\ 60
		ensure
			min = (old min + 1) \\ 60
		end

	increment_sec
		do
			sec:=(sec + 1) \\ 60
		ensure
			sec=(sec + 1) \\ 60

		end

feature

	to_str:STRING --String represantation
		local
			s:STRING
		do
			create s.make_empty
			if hour<10 then
				s.append_integer (0)
			end
			s.append_integer (hour)
			s.append_character (':')
			if min<10 then
				s.append_integer (0)
			end
			s.append_integer (min)
			s.append_character (':')
			if sec<10 then
				s.append_integer (0)
			end
			s.append_integer (sec)
			Result:=s
		end

invariant
	hour_between_0_and_23:0<=hour AND hour<=23
	min_between_0_and_59:0<=min AND min<=59
	sec_between_0_and_59:0<=sec AND sec<=59
end
