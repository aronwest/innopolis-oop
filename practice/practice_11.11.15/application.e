note
	description: "practice_11.11.15 application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit

	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
		do
				--			io.putstring ("Input string: ")
				--			io.read_line
				--			if is_palindrome (io.last_string) then
				--				io.putstring ("It is palindrome%N")
				--			else
				--				io.putstring ("It is not palindrome%N")
				--			end
			io.putstring ("Input pascal triangle's height: ")
			io.read_integer
			print_pascal_triangle (io.last_integer)
		end

feature

	print_reversed (str: STRING)
		require
			str /= Void
		do
			if str.count > 0 then
				io.put_character (str [str.count])
				print_reversed (str.substring (1, str.count - 1))
			end
		end

	is_palindrome (str: STRING): BOOLEAN
		require
			str /= Void
		local
			i, j: INTEGER
		do
				--			if str.count > 1 then
				--				str.replace_substring_all (" ", "")
				--				Result := str [1].as_lower = str [str.count].as_lower and is_palindrome (str.substring (2, str.count - 1))
				--			else
				--				Result := true
				--			end
			str.replace_substring_all (" ", "")
			Result := true
			from
				i := 1
				j := str.count
			until
				i >= j or not Result
			loop
				if str [i].as_lower /~ str [j].as_lower then
					Result := false
				end
				i := i + 1
				j := j - 1
			end
		end

	print_pascal_triangle (height: INTEGER)
		local
			i, j: INTEGER
			space: STRING
		do
			from
				i := 0
			until
				i >= height
			loop
--				space := " "
--				space.multiply (((height - i - 1) / 2).floor)
--				io.putstring (space)
				from
					j := 0
				until
					j > i
				loop
					io.putstring (pascal (i, j).out + " ")
					j := j + 1
				end
				io.new_line
				i := i + 1
			end
		end

	pascal (row, col: INTEGER): INTEGER
		do
			if col = 0 or row = col then
				Result := 1
			else
				Result := pascal (row - 1, col - 1) + pascal (row - 1, col)
			end
		end

end
