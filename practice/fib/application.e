note
	description: "fib application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit

	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
			fib: FIB
			i: INTEGER
		do
			create fib
			from
				i := 0
			until
				i > 46
			loop
				print ("Fib number " + i.out + ":" + fib.fibonacci (i).out + "%N")
				i := i + 1
			end
		end

end
