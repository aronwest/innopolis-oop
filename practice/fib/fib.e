note
	description: "Summary description for {FIB}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	FIB

feature

	fibonacci (n: INTEGER): INTEGER
		require
			n_non_negative: n >= 0
		local
			a, b, i: INTEGER
		do
			if n <= 1 then
				Result := n
			else
				a := 0
				b := 1
				Result := 0
				from
					i := 2
				invariant
					a = fibonacci (i-2)
					b = fibonacci (i-1)
				until
					i > n
				loop
					Result := a + b
					a := b
					b := Result
					i := i + 1
				variant
					n-i+1
				end
			end
		ensure
			first_is_zero: n = 0 implies Result = 0
			second_is_one: n = 1 implies Result = 1
			other_correct: n > 1 implies Result = fibonacci (n - 1) + fibonacci (n - 2)
		end

end
