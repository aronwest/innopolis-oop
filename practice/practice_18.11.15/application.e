note
	description: "practice_18.11.15 application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit

	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
		local
			int_list: SINGLY_LINKED_LIST [INTEGER]
		do
			create int_list
			across
				1 |..| 10 as i
			loop
				int_list.put_first (i.item)
			end
			io.putstring ("AFTER INSERTION: ")
			across
				1 |..| int_list.size as i
			loop
				io.putstring (int_list.get (i.item).out + " ")
			end
			io.new_line
			across
				1 |..| (int_list.size // 2) as i
			loop
				int_list.remove_first
			end
			io.putstring ("AFTER REMOVAL: ")
			across
				1 |..| int_list.size as i
			loop
				io.putstring (int_list.get (i.item).out + " ")
			end
			io.new_line
		end

end
