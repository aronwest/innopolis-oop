note
	description: "Summary description for {LIST_NODE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	LIST_NODE [T]

create
	make_with_next, make

feature {NONE}

	make (n_value: T)
		do
			value := n_value
		end

	make_with_next (n_value: T; n_next: LIST_NODE [T])
		do
			value := n_value
			next := n_next
		end

feature

	value: T

	next: LIST_NODE [T]

	set_value (n_value: T)
		do
			value := n_value
		end

	set_next (n_next: LIST_NODE [T])
		do
			next := n_next
		end

end
