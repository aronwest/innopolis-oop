note
	description: "Summary description for {PIECE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

deferred class
	PIECE

feature

	move (new_pos: POINT)
		require
			can_move (new_pos)
		do
			position:=new_pos
		end

	can_move (new_pos: POINT): BOOLEAN
		require
			false
--			new_pos /= Void
--			(1 <= new_pos.x and new_pos.x <= 8) and (1 <= new_pos.y and new_pos.y <= 8)
		deferred
		end

	colour: INTEGER

	position: POINT

feature --Constants

	BLACK: INTEGER = 0

	WHITE: INTEGER = 1

invariant
	valid_pos:(1 <= position.x and position.y <= 8) and (1 <= position.y and position.y <= 8)
	valid_colour: colour = BLACK or colour =WHITE
end
