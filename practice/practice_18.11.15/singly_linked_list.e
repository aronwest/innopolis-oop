note
	description: "Summary description for {SINGLY_LINKED_LIST}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	SINGLY_LINKED_LIST [T]

feature

	size: INTEGER

	put_first (value: T)
		require
			value_exists: value /= Void
		do
			head := create {LIST_NODE [T]}.make_with_next (value, head)
			size := size + 1
		ensure
			size = old size + 1
		end

	remove_first
		require
			size > 0
		do
			head := head.next
			size := size - 1
		ensure
			size = old size - 1
		end

	get (index: INTEGER): T
		require
			1<=index and index <= size
		local
			i: INTEGER
			cur: LIST_NODE [T]
		do
			from
				i := 1
				cur := head
			until
				i >= index
			loop
				cur := cur.next
				i := i + 1
			end
			Result := cur.value
		end

feature {NONE} --Implementation

	head: LIST_NODE [T]

		--	tail: detachable LIST_NODE [T]

invariant
	size >= 0

end
