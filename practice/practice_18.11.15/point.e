note
	description: "Summary description for {POINT}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	POINT

create
	make

feature {NONE}

	make (p_x, p_y: INTEGER)
		do
			x := p_x
			y := p_y
		ensure
			x = p_x
			y = p_y
		end

feature

	x, y: INTEGER

end
