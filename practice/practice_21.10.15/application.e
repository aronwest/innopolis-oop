note
	description: "project_21.10.15 application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit

	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	l: LINKED_LIST [INTEGER]

--	strs:LINKED_LIST[STRING]

	make
		local
			is_even, is_non_negative, is_negative: PREDICATES [INTEGER]
			-- Run application.

			begins_with:BEGINS_WITH
		do
			--21.10
			create l.make
			l.put_front (-5)
			l.put_front (-3)
			l.put_front (-7)
			l.put_front (-8)
			l.put_front (-10)
			create is_even.make (create {EVEN})
			create is_non_negative.make (create {NON_NEGATIVE})
			create is_negative.make (create {NEGATIVE})
			print (is_even.collect (l).count.out + "%N")
			print (is_non_negative.collect (l).count.out + "%N")
			print (is_even.has (l).out + "%N")
			print (is_non_negative.has (l).out + "%N")
			print (is_negative.forall (l).out + "%N")

			--28.10
			create begins_with

		end

end
