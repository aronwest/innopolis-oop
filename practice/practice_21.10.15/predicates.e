note
	description: "Summary description for {PREDICATES}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PREDICATES [T]

create
	make

feature --Initialization

	make (init_predicate: UNARY_PREDICATE [T])
		require
			predicate_exists: init_predicate /= Void
		do
			predicate := init_predicate
		end

feature {NONE}

	predicate: UNARY_PREDICATE [T]

feature --Predicate functions

	collect (list: LIST [T]): LIST [T]
		require
			list_exists: list /= Void
		local
			new_list: LINKED_LIST [T]
		do
			create new_list.make
			across
				list as i
			loop
				if predicate.test (i.item) then
					new_list.force (i.item)
				end
			end
			Result := new_list
		ensure
			result_exists: Result /= Void
			result_not_more_than_arg: list.count >= Result.count
			result_is_subset_of_arg: across Result as i all list.has (i.item) end
		end

	has (list: LIST [T]): BOOLEAN
		require
			list_exists: list /= Void
		do
			Result := collect (list).count /= 0
		ensure
			Result implies across list as i some predicate.test (i.item) end
			not Result implies not across list as i some predicate.test (i.item) end
		end

	forall (list: LIST [T]): BOOLEAN
		require
			list_exists: list /= Void
		do
			Result:=collect (list).count = list.count
		ensure
			Result implies across list as i all predicate.test (i.item) end
			not Result implies not across list as i all predicate.test (i.item) end
		end

invariant
	predicate_exists: predicate /= Void

end
