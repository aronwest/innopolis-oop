note
	description: "Summary description for {MY_PREDICATE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

deferred class
	UNARY_PREDICATE [T]

feature

	test (value: T): BOOLEAN
		deferred
		end

end
