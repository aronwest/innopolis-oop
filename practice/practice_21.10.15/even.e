note
	description: "Summary description for {EVEN}."
	author: "Timur Kasatkin"
	date: "$Date$"
	revision: "$Revision$"

class
	EVEN

inherit

	UNARY_PREDICATE [INTEGER]

feature

	test (value: INTEGER): BOOLEAN
		do
			Result := value \\ 2 = 0
		ensure then
			result_valid: Result = (value \\ 2 = 0)
		end

end
