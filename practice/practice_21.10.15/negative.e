note
	description: "Summary description for {NEGATIVE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	NEGATIVE

inherit

	UNARY_PREDICATE [INTEGER]

feature

	test (value: INTEGER): BOOLEAN
		do
			Result := value < 0
		ensure then
			result_valid: Result = (value < 0)
		end

end
