note
	description: "Summary description for {BEGINS_WITH}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	BEGINS_WITH

inherit

	BINARY_PREDICATE [CHARACTER, STRING]

feature

	test (val1: CHARACTER; val2: STRING): BOOLEAN
		require else
			val2_exists: val2 /= Void
		do
			Result := val2.starts_with (val1.out)
		ensure then
			Result = val2.starts_with (val1.out)
		end

end
