note
	description: "Summary description for {BINARY_PREDICATE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

deferred class
	BINARY_PREDICATE [T1, T2]

feature

	test (val1: T1; val2: T2): BOOLEAN
		deferred
		end

	

end
