note
	description: "Summary description for {NON_NEGATIVE}."
	author: "Timur Kasatkin"
	date: "$Date$"
	revision: "$Revision$"

class
	NON_NEGATIVE

inherit

	UNARY_PREDICATE [INTEGER]

feature

	test (value: INTEGER): BOOLEAN
		do
			Result := value >= 0
		ensure then
			result_valid: Result = (value >= 0)
		end

end
