note
	description : "fib application root class"
	date        : "$Date$"
	revision    : "$Revision$"

class
	FIBONACCI

inherit
	ARGUMENTS

create
	calc

feature {NONE} -- Initialization

	calc
			-- Run application.
		do
			--| Add your code here
			print ("Hello Eiffel World!%N")
		end

end
