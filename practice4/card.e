note
	description: "Summary description for {CARD}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CARD

create
	make

feature

	number: INTEGER

	color: CHARACTER

	make (card_num: INTEGER; card_color: CHARACTER)
		require
			card_num_between_2_and_10: 2 <= card_num and card_num <= 10
			color_one_out_four_possible: is_valid_color (card_color)
		do
			number := card_num
			color := card_color
		ensure
			number = card_num
			color = card_color
		end

	is_valid_color (card_color: CHARACTER): BOOLEAN
		do
			Result := card_color ~ 'R' or card_color ~ 'W' or card_color ~ 'G' or card_color ~ 'B'
		end

end
