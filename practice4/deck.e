note
	description: "Summary description for {DECK}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	DECK

create
	make

feature

	make
		do
			create cards.make
		end

	put (card: CARD)
		require
			card_exists: card /= Void
			deck_does_not_contain_card: not cards.has (card)
		do
			cards.put (card)
		ensure
			deck_contains_card:cards.has (card)
		end

	top_card:CARD
	do
		Result:=cards.item
		cards.remove
	end

	cards: LINKED_STACK [CARD]

invariant
	cards_size_not_more_than_36: cards.count <= 36

end
